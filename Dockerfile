FROM ubuntu

RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && ln -fs /usr/share/zoneinfo/Europe/Paris/etc/localtime \
    && apt-get install -y tzdata \
    && dpkg-reconfigure --frontend noninteractive tzdata \
    && apt-get install zip wget php-cli php-zip unzip npm build-essential python -y \
    && wget -O composer-setup.php https://getcomposer.org/installer \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && npm install -g sass \
    && npm install -g webpack \
    && npm install -g sass-loader

WORKDIR /src/