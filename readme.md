# Ubuntu in Docker with Tools pour Alpes Isère Habitat

Fichier de références pour la mise à jour de l'image Docker Ubuntu avec quelques outils régulièrement utilisés.

## Mettre à jour l'image

Editer le fichier Dockerfile, en ajoutant les modules et extentions manquantes.
Il suffit ensuite de faire un build de l'image, et la publier sur le hub docker en lui précisant un numéro de version (à adapter avec vos infos du Hub Docker ):

```
$ docker build -t alpesiserehabitat/aih-tools .
$ docker tag {id du build} alpesiserehabitat/aih-tools:{Version}
$ docker login
$ docker push alpesiserehabitat/aih-tools:{Version}
```
## Utilisation de l'image

Cette commande permet de monter le repertoire courant dans le repertoire /src du containeur.

```
$ docker run -v "$(pwd)":/src -it alpesiserehabitat/aih-tools:1.0
```

## Authors

* **Yoan Bernabeu** - *pour Alpes Isère Habitat* - [Profil Gitlab](https://gitlab.com/yoan.bernabeu)

## License

This project is licensed under the MIT License - see the [LICENCE](LICENCE) file for details